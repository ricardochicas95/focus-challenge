terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.10.1"
    }
  }
}

resource "digitalocean_kubernetes_cluster" "test-focus" {
  name = "test-focus"
  region = "nyc1"
  version = "1.20.8-do.0"

  node_pool {
    name = "test-nodes"
    size = "s-2vcpu-2gb"
    node_count = 1
  }
}