provider "kubernetes" {
  config_path = "${local_file.kubernetes_config.filename}"
}

resource "kubernetes_service" "test-focus" {
  metadata {
    name = "test-focus"
  }
  spec {
    selector = {
      app = kubernetes_pod.test-focus.metadata.0.labels.app
    }
    session_affinity = "ClientIP"
    port {
      port = 80
      node_port = 30000
      target_port = 80
    }
    type = "NodePort"
  }
}

resource "kubernetes_pod" "test-focus" {
  metadata {
    name = "test-focus"
    labels = {
      app = "test-focus"
    }
  }

  spec {
    container {
      image = "kiore/test-focus"
      name  = "test-focus"
    }
  }
}