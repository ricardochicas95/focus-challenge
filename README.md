#  terraform_k8s_test
This repo holds the assets needed for our Terraform, Kubernetes, And Helm coding test

## Test Overview
The purpose of this test is to demonstrate your knowledge in the following areas: 
* Digitalocean Provider
* Digitalocean container registry 
* Terraform
* Kubernetes [Digitalocean kubernetes service] 
* Helm
* Gitlab CI 

This repo holds the application code and Dockerfile in the "application" directory. The helm chart to be used to deploy the application to the Kubernetes cluster is the "charts" directory. 

## Repository Content
Terraform files that create kubernetes cluster, services and pods, config files to mount the image in the cluster and expose the ports and deploy.

The Helm chart contains all needed configurations to create a service in kubernetes cluster in Digital Ocean with the docker image.

## Usage

### Terraform

To run terraform in this project you need a kubeconfig file, see the attached file in the email.

First create a folder as '.kube' in root directory and a new config file as 'config' and copy the content of the file attached and paste it into the new one you've created.

Open your terminal in terraform directory and run:

```bash
~$ terraform init
~$ terraform plan
~$ terraform apply
```

### Helm

Open your terminal in _focus-challenge/charts/test-focus_ and run:

First check if a service is running
```bash
~$ helm list
```
If not run to create the service in kubernetes cluster
```bash
~$ helm install <NAME> ./
```
then check again if a service is running.

Last, run this command to refresh the cluster with any change you made. 
```bash
~$ helm upgrade <NAME> .
```

To check if changes were applied run.
```bash
~$ kubectl get services -o wide
~$ kubectl get nodes -o wide
~$ kubectl get pods -o wide
```

To run the app in local machine run

```bash
~$ kubectl port-forward <POD NAME> 8080:80
```